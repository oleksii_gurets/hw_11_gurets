<?php
class Person{
    public $fullName = '';
    public $phone = '';
    public $email = '';
    public $role = '';

    public function __construct($fullName, $phone, $email, $role){
        $this->fullName = $fullName;
        $this->phone = $phone;
        $this->email = $email;
        $this->role = $role;
    }

    public function getVisitCard(){
        return '<div class="col-sm-4">
                    <div class="card text-white bg-secondary mb-3">
                        <div class="card-header">
                            Визитная карточка
                        </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><strong>Ф.И.О.: </strong>'. $this->fullName .'</li>
                        <li class="list-group-item"><strong>Номер телефона: </strong>'. $this->phone .'</li>
                        <li class="list-group-item"><strong>Электронная почта: </strong>'. $this->email .'</li>
                        <li class="list-group-item"><strong>Статус резидента: </strong>' . $this->role . '</li>
        ';
    }
}
?>