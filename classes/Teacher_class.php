<?php
class Teacher extends Person{
    public $subject = '';

    public function __construct($fullName, $phone, $email, $role, $subject){
        parent::__construct($fullName, $phone, $email, $role);
        $this->subject = $subject;
    }

    public function getVisitCard(){
        return parent::getVisitCard() . '<li class="list-group-item"><strong>Преподаваемый предмет: </strong>' . $this->subject . '</li>
                                    </ul>
                                </div>
                            </div>
        ';
    }
}
?>