<?php
class Administrator extends Person{
    public $workingDay = '';

    public function __construct($fullName, $phone, $email, $role, $workingDay){
        parent::__construct($fullName, $phone, $email, $role);
        $this->workingDay = $workingDay;
    }

    public function getVisitCard(){
        return parent::getVisitCard() . '<li class="list-group-item"><strong>Рабочий день: </strong>' . $this->workingDay . '</li>
                                    </ul>
                                </div>
                            </div>
        ';
    }
}
?>