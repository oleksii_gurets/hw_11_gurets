<?php
class Student extends Person{
    public $averageMark = 0.0;

    public function __construct($fullName, $phone, $email, $role, $averageMark){
        parent::__construct($fullName, $phone, $email, $role);
        $this->averageMark = $averageMark;
    }

    public function getVisitCard(){
        return parent::getVisitCard() . '<li class="list-group-item"><strong>Средний балл успеваемости: </strong>' . $this->averageMark . '</li>
                                    </ul>
                                </div>
                            </div>
        ';
    }
}
?>