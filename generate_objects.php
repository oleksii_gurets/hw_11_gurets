<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/database_files/connection_db.php";

try {
    $sql = "SELECT * FROM members";          // Получаем данные из базы
    $dbResponse = $dbConnect->query($sql);
    $membersArray = $dbResponse->fetchAll(); // Формируем массив на основе полученных данных
} catch (Exception $errorMembers) {
    echo 'Error getting members data!<br>';
    echo $errorMembers->getMessage();
    echo '<br><a href="/database_files/create_db.php">Создать таблицу</a>';
    die();
}

$membersObjects = [];                         // На основе полученного массива из базы создаем нужные объекты классов
foreach ($membersArray as $memberObject) {
    switch ($memberObject['member_role']) {
        case 'студент':
            $membersObjects[] = new Student($memberObject['full_name'], $memberObject['phone'], $memberObject['email'],
            $memberObject['member_role'], $memberObject['average_mark']);
            break;
        case 'преподаватель':
            $membersObjects[] = new Teacher($memberObject['full_name'], $memberObject['phone'], $memberObject['email'],
            $memberObject['member_role'], $memberObject['subject_type']);
            break;
        case 'администратор':
            $membersObjects[] = new Administrator($memberObject['full_name'], $memberObject['phone'], $memberObject['email'],
            $memberObject['member_role'], $memberObject['working_day']);
            break;     
    }
}
?>