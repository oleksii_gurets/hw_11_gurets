<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/database_files/connection_db.php";
$membersTest = [                            // Создаем массив с тестовыми данными для заполнения созданной таблицы базы данных
    [
        'full_name' => 'Рогозин Павел Петрович',
        'phone' => '097-655-44-22',
        'email' => 'rogozin@ukr.net',
        'member_role' => 'студент',
        'average_mark' => 4.7
    ],
    [
        'full_name' => 'Шишкина Ольга Романовна',
        'phone' => '096-110-11-01',
        'email' => 'shishkina@gmail.com',
        'member_role' => 'студент',
        'average_mark' => 5.0
    ],
    [
        'full_name' => 'Строгая Ирина Олеговна',
        'phone' => '091-119-19-17',
        'email' => 'strogaya@ukr.net',
        'member_role' => 'преподаватель',
        'subject_type' => 'история'
    ],
    [
        'full_name' => 'Проба Василий Тимурович',
        'phone' => '089-889-98-88',
        'email' => 'proba@gmail.com',
        'member_role' => 'администратор',
        'working_day' => 'четверг'
    ],
    [
        'full_name' => 'Долгова Анна Владиславовна',
        'phone' => '099-456-13-13',
        'email' => 'dolgova@ukr.net',
        'member_role' => 'преподаватель',
        'subject_type' => 'химия'
    ],
    [
        'full_name' => 'Туренко Роман Николаевич',
        'phone' => '033-333-33-24',
        'email' => 'turenko@gmail.com',
        'member_role' => 'студент',
        'average_mark' => 3.8
    ],
    [
        'full_name' => 'Горленко Софья Никодимовна',
        'phone' => '011-777-88-99',
        'email' => 'gorlenko@gmail.com',
        'member_role' => 'студент',
        'average_mark' => 4.1
    ]
];
try {
    foreach ($membersTest as $testMember) {                // Создаем строку для запроса в зависимости от роли записи
        switch ($testMember['member_role']) {
            case 'студент':
                $addString = "average_mark=".$testMember['average_mark']."";
                break;
            case 'преподаватель':
                $addString = "subject_type='".$testMember['subject_type']."'";
                break;
            case 'администратор':
                    $addString = "working_day='".$testMember['working_day']."'";
                    break;
        }
                                                         /*Формируем запрос, добавляя нужную нам строку по контексту*/
        $sql = "INSERT INTO members SET    
        full_name='".$testMember['full_name']."',
        phone='".$testMember['phone']."',
        email='".$testMember['email']."',
        member_role='".$testMember['member_role']."',".$addString."";
        $dbConnect->exec($sql);
        
    }
} catch (Exception $membersError) {
    die('Test data adding error!' . '<br>' . $membersError->getMessage());
}
echo 'Database table was filled with 7 entries<br>';
echo '<a href="/">На главную</a>';
?>