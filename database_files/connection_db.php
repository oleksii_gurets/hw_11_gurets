<?php
try {
    $dbConnect = new PDO('mysql:host=localhost;dbname=members', 'root', 'root');
    $dbConnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbConnect->exec('SET NAMES "utf8"');
} catch (Exception $errorAccess) {
    echo 'No database access!';
    echo $errorAccess->getMessage();
    die();
}
?>