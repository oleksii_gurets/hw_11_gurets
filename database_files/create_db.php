<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/database_files/connection_db.php";
try {
    $sql = 'CREATE TABLE members (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        full_name VARCHAR(255),
        phone VARCHAR(255),
        email VARCHAR(255),
        member_role VARCHAR(255),
        average_mark FLOAT(10,1),
        subject_type VARCHAR(255),
        working_day VARCHAR(255)
        ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;';
    $dbConnect->exec($sql);
} catch(Exception $errorTable) {
    echo 'Error creating TABLE: members<br>';
    echo $errorTable->getMessage();
    echo '<br><a href="/">На главную</a>';
    die();
}
echo 'TABLE members created succesfully!<br>';
echo '<a href="/">На главную</a>';
die();
?>