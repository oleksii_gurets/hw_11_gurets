<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/database_files/connection_db.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/classes/Person_class.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/classes/Student_class.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/classes/Teacher_class.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/classes/Administrator_class.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/generate_objects.php";
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Домашнее задание №11 Гурца Алексея</title>
    <meta name="description" content="">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

</head>

<body>
    <div class="container-fluid">
        <header class="p-3 m-3 text-warning" style="background-color: #4b6477;">
            <div class="text-center">
                <h1>Домашнее задание №11</h1>
                <h2>База данных - введение </h2>
            </div>
        </header>
    </div>
    <div class="container">
        <div class="row">
            <nav class="nav">
                <a class="nav-link" href="/database_files/create_db.php">Создать таблицу базы данных</a>
                <a class="nav-link" href="/database_files/seeder_db.php">Заполнить таблицу тестовыми данными</a>
            </nav>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <?php if(!empty($membersObjects)): ?>
                <?php foreach($membersObjects as $memberObject): ?>
                <?=$memberObject->getVisitCard(); ?>
                <?php endforeach;?>
            <?php endif; ?>
        </div>
    </div>
    <div class="container-fluid">
        <footer class="d-flex justify-content-center p-1 m-3 text-warning" style="background-color: #4b6477;">
            <p>Гурец Алексей &copy;2021 <a href="mailto:oleksii.gurets@gmail.com" class="text-warning">Все вопросы по
                    почте</a> <a href="/" class="text-warning">Главная</a></p>
        </footer>
    </div>
</body>

</html>